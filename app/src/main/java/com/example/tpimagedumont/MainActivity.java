package com.example.tpimagedumont;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private final ActivityResultLauncher<String> launcher = registerForActivityResult(new ActivityResultContracts.GetContent(),
            uri -> {
                if (uri != null) {
                    TextView textView = findViewById(R.id.path);
                    textView.setText(uri.toString());
                    chargerImage(uri);
                }
    });

    private void chargerImage(Uri uri) {
        ImageView imageView = findViewById(R.id.image);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;
        try {
            Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, options);
            imageView.setImageBitmap(bitmap);
        } catch (Exception e) {
            Toast.makeText(this, R.string.load_error, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(findViewById(R.id.title_bar));
        registerForContextMenu(findViewById(R.id.image));
    }

    public void onClick(View view) {
        if (view.getId() == R.id.open) {
            launcher.launch("image/*");
        } else if (view.getId() == R.id.reset) {
            TextView textView = findViewById(R.id.path);
            chargerImage(Uri.parse(textView.getText().toString()));
        }
    }

    public boolean onClick(MenuItem item) {
        ImageView imageView = findViewById(R.id.image);
        if(imageView.getDrawable() == null) {
            return false;
        }
        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        Bitmap bitmap2 = null;

        if (item.getItemId() == R.id.v_miror) {
             bitmap2 = miroirVertical(bitmap);
        } else if (item.getItemId() ==  R.id.h_miror) {
            bitmap2 = miroirHorizontal(bitmap);
        } else if (item.getItemId() ==  R.id.inv_colors) {
            bitmap2 = inverserCouleurs(bitmap);
        } else if (item.getItemId() ==  R.id.to_grey) {
            bitmap2 = toGrey(bitmap);
        } else if (item.getItemId() ==  R.id.rotate) {
            bitmap2 = rotattionHoraire(bitmap);
        } else if (item.getItemId() ==  R.id.rotate_c) {
            bitmap2 = rotattionAntiHoraire(bitmap);
        }

        if (bitmap2 != null) {
            imageView.setImageBitmap(bitmap2);
        }
        return true;
    }
    private Bitmap miroirVertical(Bitmap bitmap) {

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap bitmap2 = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        for (int i = 0; i < height; i++) {
            int[] pixels = new int[width];
            bitmap.getPixels(pixels, 0, width, 0, i, width, 1);
            bitmap2.setPixels(pixels, 0, width, 0, height - i - 1, width, 1);
        }
        return bitmap2;
    }

    private Bitmap miroirHorizontal(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap bitmap2 = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        for (int i = 0; i < height; i++) {
            int[] pixels = new int[width];
            bitmap.getPixels(pixels, 0, width, 0, i, width, 1);
            for (int j = 0; j < width; j++) {
                bitmap2.setPixel(width - j - 1, i, pixels[j]);
            }
        }
        return bitmap2;
    }

    private Bitmap inverserCouleurs(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap bitmap2 = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int pixel = bitmap.getPixel(j, i);
                bitmap2.setPixel(j, i,
                        android.graphics.Color.argb(
                        255,
                        255 - android.graphics.Color.red(pixel),
                        255 - android.graphics.Color.green(pixel),
                        255 - android.graphics.Color.blue(pixel)
                    )
                );
            }
        }
        return bitmap2;
    }

    private Bitmap toGrey(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap bitmap2 = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        for (int i = 0; i < height; i++) {
            int[] pixels = new int[width];
            bitmap.getPixels(pixels, 0, width, 0, i, width, 1);
            for (int j = 0; j < width; j++) {
                int pixel = pixels[j];
                int grey = (int) ((android.graphics.Color.red(pixel) + android.graphics.Color.green(pixel) + android.graphics.Color.blue(pixel)) / 3);
                bitmap2.setPixel(j, i, android.graphics.Color.argb(255, grey, grey, grey));
            }
        }
        return bitmap2;
    }

    private Bitmap rotattionHoraire(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap bitmap2 = Bitmap.createBitmap(height, width, Bitmap.Config.ARGB_8888);
        for (int i = 0; i < height; i++) {
            int[] pixels = new int[width];
            bitmap.getPixels(pixels, 0, width, 0, i, width, 1);
            for (int j = 0; j < width; j++) {
                bitmap2.setPixel(height - i - 1, j, pixels[j]);
            }
        }
        return bitmap2;
    }

    private Bitmap rotattionAntiHoraire(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap bitmap2 = Bitmap.createBitmap(height, width, Bitmap.Config.ARGB_8888);
        for (int i = 0; i < height; i++) {
            int[] pixels = new int[width];
            bitmap.getPixels(pixels, 0, width, 0, i, width, 1);
            for (int j = 0; j < width; j++) {
                bitmap2.setPixel(i, width - j - 1, pixels[j]);
            }
        }
        return bitmap2;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }
}